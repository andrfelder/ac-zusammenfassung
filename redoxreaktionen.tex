\section{Redox-Reaktionen}
Unter Redoxreaktionen versteht man Reaktionen, bei denen Elektronen von einer Verbindung (oder einem lon) auf eine andere Verbindung (oder ein lon) übertragen werden. Eine Redoxreaktion ist also eine simultane Abgabe und Aufnahme von Elektronen durch die Reaktanden.
\\
Hier als Beispiel mit der Reaktion: \ce{Cu^{2+}_{(aq)} + Zn_{(s)} -> Cu_{(s)} + Zn^{2+}_{(aq)}}:
\begin{figure}[!ht]
    \centering
    \begin{minipage}{1\linewidth}
      \includegraphics[width=\linewidth]{Media/redox1.jpg}
    \end{minipage}
\end{figure}
\subsection{Oxidationszahlen}
Jedem Atom wird eine formale Ladung zugeordnet. Bei ionischen Verbindungen wie z.B. NaCl hat Natrium die Oxidationszahl (+1)
und Chlor die Oxidationszahl (-1), da sowohl im Festkörper und in Lösung diese nachgewiesen werden können.
\begin{shaded}
	Für kovalente Bindungen verwendet man folgende Regeln für die Bestimmung von Oxidationszahlen:
    \begin{enumerate}
        \item Jedes Atom im elementaren Zustand besitzt die Oxidationszahl Null.
        \item Die Summe der Oxidationszahlen aller Atome einer Verbindung ist gleich der Gesammtladung der Verbindungen.
        \item In Molekülen mit zwei oder mehr Arten von Elementen werden die Bindungselektronen formell ganz dem Element mit der grösseren Elektronegativität zugeordnet und es erhält eine negative Oxidationszahl, das Atom mit der kleineren Elektronegativität erhält eine positive Oxidationszahl.
        \item Die Berechnung erfolgt nun durch Addition:
        \begin{itemize}
            \item -1 für jede Bindung zu einem elektropositiveren Atom
            \item 0 für jede Bindung zu einem gleichen Atom
            \item +1 für jede Bindung zu einem elektronegativerem Atom.
            \item Doppel- und Dreifachbindungen werden doppelt bzw. dreifach gezählt
        \end{itemize}
        \item Für ein C Atom ergibt sich die Oxidationszahl durch folgende Werte:
        \begin{itemize}
            \item -1 für jedes anhängende H-Atom
            \item 0 für jedes anhängende C-Atom
            \item +1 für jede Bindung zu einem Heteroatom wie O, N, S etc.
        \end{itemize}
        \item Fluor hat in allen Verbindungen immer -1
        \item Sauerstoff hat im Allgemeinen immer -2 ausser in \ce{OF2} (+2) oder in Peroxiden wie z.B. \ce{H2O2} (-1). 
        \item Wasserstoff hat meistens +1 ausser bei Metallhydriden wie z.B. \ce{NaH} (-1)
    \end{enumerate}
\end{shaded}
\subsection{Rezept zum Aufstellen von Redoxgleichungen}
Das Aufstellen von Redoxreaktionen kann man nach folgendem Rezept machen:
\begin{enumerate}
    \item Zerlegen der Verbindungen in die kleinsten in Lösung vorkommenden Teilchen.
    \item Alle Oxidationszahlen bestimmen. Zuordnen was oxidiert und was reduziert wird.
    \item Halbzellenreaktionen aufstellen und nicht beteiligte Teilchen ignorieren.
    \item Ladungs- und Teilchenausgleich, sodass die Ladung und Teilchenzahl auf beiden Seiten stimmt.
    \item Kleinstes gemeinsames Vielfaches der Elektronenzahl bestimmen und Linearkombinationen der Halbreaktionen bilden.
    \item Unbeteiligte Teilchen hinzunehmen
    \item Kontrolle von Ladung und Teilchenzahl.
\end{enumerate}
Beachte, in wässriger Lösung muss man auf den pH achten. Im basischen wird mit \ce{OH^-} und \ce{H2O}, im sauren mit \ce{H^+} und \ce{H2O} ausgeglichen.
\subsection{Thermodynamik von Redoxreaktionen}
Eine allgemeine Gesetzmässigkeit ist, dass alles den energieärmsten Zustand anstrebt. Bei einer chemischen Rekation findet
neben Stoffumsätzen auch ein Energieumsatz statt, welcher durch die Änderung der Enthalpie $\Delta H$ gekennzeichnet ist.
Wenn $\Delta H$ negativ ist, so ist es eine exotherme Reaktion und läuft unter Enthalpiegewinn ab. Ist $\Delta H$ positiv,
spricht man von einer endothermen Reaktion und läuft unter Enthalpieverlust ab.\\
\begin{shaded}
    Beide Bestimmungsfunktionen für die Spontanität können in der folgenden Gleichung zusammengefasst werden:
    \begin{equation}
        \Delta G = \Delta H - T\Delta S 
    \end{equation}
    Dies wird die freie Enthalpie genannt. Ist die Änderung der freien Enthalpie negativ, so wird die Reaktion als exergon bezeichnet und läuft spontan ab. Ist sie hingegen positiv, wird die Reaktion als endergon bezeichnet und läuft nicht spontan ab.
\end{shaded}
\subsection{Galvanische Zelle und Redoxpotentiale}
Betrachte die Reaktion \ce{Zn + Cu^{2+} -> Zn^{2+} + Cu}. Trennt man diese Reaktion räumlich und verbindet diese mit einer Brücke spricht man von einer galvanischen Zelle:
\begin{figure}[!ht]
    \centering
    \begin{minipage}{0.8\linewidth}
      \includegraphics[width=\linewidth]{Media/zelle.jpg}
    \end{minipage}
\end{figure}
Die treibende Kraft wird elektromotorische Kraft (EMK) genannt und entspricht der Potentialdifferenz der beiden Halbzellen.
\begin{shaded}
    Das elektrochemische Potential eines galvanischen Elemnts entspricht der Änderung der freien Enthalpie pro Mol überführter Elektronen:
    \begin{equation}
        \Delta_R G^0 = -z \cdot F \cdot \Delta E^0
    \end{equation}
    wobei $z$ Anzahl der Mol umgesetzter \ce{e^-}, $F$ die Farraday Konstante und das hochgestellte 0 Standartbedienungen bedeuten.
    Ist die freie Enthalpie negativ, so ist die elektrochemische Potentialdifferenz positiv und die Reaktion läuft spontan ab.
\end{shaded}
\subsection{Berechnung elektrochemischer Potentiale}
Aus der freien Enthalpie der Redoxreaktion lässt sich für das Daniell-Element eine Potentialdifferenz von 1.1V unter Standartbedienungen berechnen:
\begin{equation}
    \Delta E^0 = \frac{\Delta_R G^0}{-zF}=\frac{-212 kJ \cdot mol^{-1}}{-2F} = \text{+1.1V}
\end{equation}
Ist die freie Enthalpie nicht bekannt, so lässt sich diese durch Verknüpfen der freien Enthalpien der Halbzellenreaktionen
berechnen. Es gilt:
\begin{equation}
    \Delta_R G^0 = \sum n_i \Delta_R G^0 \text{(Produkte)} - \sum n_{ii} \Delta_R G^0 \text{(Edukte)}
\end{equation}
wobei $n_i$ und $n_{ii}$ die Stöchiometriezahlen sind. \\
Das Standardpotential einer Halbzelle ist eine intensive Grösse, ändert sich also nicht mit der Grösse des Systems und steht mit den extensiven Grössen der freien Enthalpie und der
Anzahl der übertragenen Elektronen in folgender Beziehung:
\begin{equation}
    \Delta_R G^0 = -z \cdot F \cdot E^0 \Leftrightarrow E^0 = \frac{-\Delta_R \cdot G^0}{z \cdot F}
\end{equation}
\begin{shaded}
    Es gilt für die gesammte Zelle:
    \begin{equation}
        \Delta E^0 = E^0\text{(Red)} - E^0\text{(Ox)}
    \end{equation}
    Somit können Potentialdifferenzen galavnischer Zellen aus den tabellierten Standard-Reduktionspotentialen der Halbzellenreaktionen berechnet werden.
    Wichtig dabei ist: Dabei dürfen nur Halbzellenpotentiale von anderen abgezogen werden, wenn die entsprechenden Halbzellenreaktionen eine richtige Gesamtzellenreaktion mit richtigen Ausgleich von Elektronenaufnahme und Elektronenabgabe ergibt.
\end{shaded}
\subsection{Standard-Reduktionspotentiale}
So wie eine Redoxreaktion als Summe von zwei Halbzellenreaktionen geschrieben werden kann, ist es auch möglich, die EMK einer Zelle als die Summe von zwei Halbzellenpotentialen anzusehen.
Diese kann jedoch nicht induviduell bestimmt werden, deshalb benutzt man eine Bezugshalbzelle, welcher der Wert 0 zugeordnet wurde:
\begin{shaded}
    Die Standard-Halbzelle ist besteht aus einer Platinelektrode, an der die Reaktion:
    \begin{equation}
        \ce{2 H3O^+ + 2e^- -> H2 + H2O} \quad \ce{E^0 = 0.00V}
    \end{equation}
    abläuft bei Standardbedienungen.
\end{shaded}
Bei der Bestimmung von Halbzellenpotentialen betrachtet man die Reaktionen, bei der Wasserstoff oxidiert und die Substanz, deren Redoxpotential zu bestimmen ist, reduziert wird. So erhält man Standard-Reduktionspotentiale \ce{E^0}.
\subsection{Das Latimer-Diagramm}
Für Elemente, die in mehreren Oxidationsstufen vorkommen, werden die Reduktionspotentiale der einzelnen Halbzellenreaktionen häufig graphisch in Latimer- Diagrammen dargestellt.
Viele Redoxreaktionen sind pH-Abhängig, daher ist es immer wichtig darauf zu schauen, ob das Latimer-Diagramm auch im Sauren oder Basischen ist.
\subsubsection{Berechnung fehlender Standard-Reduktionspotentiale}
Betrachte folgendes Latimer-Diagramm:
\begin{figure}[!ht]
    \centering
    \begin{minipage}{.8\linewidth}
        \includegraphics[width=\linewidth]{Media/latimer.jpg}
    \end{minipage}
\end{figure}
Um die Potentiale über mehrere Stufen aus dem Latimer-Diagramm zu berechnen, werden die Produkte aus den Standardpotentialen und der Anzahl Elektronen jeder Stufe addiert und diese Summe durch die Gesammtzahl übertragener Elektronen für die Umwandlung dividiert:
\begin{equation}
    \ce{E^0 = \frac{z^'E^{0'} + z^{''}E^{0''}}{z^'+z^{''}}}
\end{equation}
\subsection{Nernst Gleichung}
Bis jetzt wurden die Redoxpotentiale für Reaktionen berechnet, bei denen alle Reaktanden im Standardzustand vorliegen. Bei Abweichungen der Bedingungen (Konzentration, Druck, Temperatur) von den Standardbedingungen kann die Nernst- Gleichung zur Umrechnung des Standard-Reduktionspotentials \ce{E^0} in das Potential \ce{E} verwendet werden.
\begin{shaded}
    Betrachten wir den allgemeinen Fall einer z-Elektronenreduktion einer Spezies Ox zu einer Spezies Red:
    \begin{equation}
        \ce{Ox + z e^- -> Red}
    \end{equation}
    Nun kann man die \textbf{Nernst Gleichung} verwenden:
    \begin{equation}
        \ce{E} = \ce{E^0} - \frac{RT}{zF} \ln\left(\frac{\ce{[Red]}}{\ce{[Ox]}}\right)
    \end{equation}
    oder vereinfacht:
    \begin{equation}
        \ce{E} = \ce{E^0} - \frac{0.05914}{z} \cdot \log\left(\frac{\ce{[Red]}}{\ce{[Ox]}}\right) \quad \text{Bei 298K}
    \end{equation}
    Mit der Gleichung können auch Redoxpotentiale direkt berechnet werden:
    \begin{equation}
        \ce{\Delta E} = \ce{\Delta E^0} - \frac{RT}{zF}\ln\left(\ce{\frac{[Ox2] \cdot [Red1]}{[Red2] \cdot [Ox1]}}\right)
    \end{equation}
    oder vereinfacht:
    \begin{equation}
        \ce{\Delta E} = \ce{\Delta E^0} - \frac{0.05914}{z}\log\left(\ce{\frac{[Ox2] \cdot [Red1]}{[Red2] \cdot [Ox1]}}\right)
    \end{equation}
\end{shaded}
\subsection{Elektrolyse}
Durch ein externes Potential (Stromquelle) werden dabei gegen das natürliche Gefälle von \ce{\Delta G} Redox-Reaktionen erzwungen. Elektrische Energie wird in chemische Energie umgewandelt.
Mehrere Konzepte welche im Heft gefunden werden können können im \textbf{Faraday'schen Gesetz} zusammengefasst werden, welches erlaubt die Masse einer abgeschiedenen Substanz während der Elektrolyse zu berechnen:
\begin{equation}
    m = \frac{Q \cdot M}{F \cdot z}
\end{equation}
\subsection{Redoxtitration}
Ähnlich wie bei einer Säure-Base Titration kann man auch redoxaktive Ionen durch Titration mit einem Oxidationsmittel oder Reduktionsmittel quantitativ bestimmen. Dabei wird die zu bestimmende Substanz tropfenweise mit einer Masslösung des Oxidations- bzw. Reduktionsmittels solange umgesetzt, bis sie vollständig reduziert bzw. oxidiert ist.
\subsubsection{Iodometrie}
Die Iodometrie beruht auf dem Redoxgleichgewicht:
\begin{equation}
    \ce{2 I^- <=> I2 + 2e^-}
\end{equation}
Dabei lassen sich Oxidationsmittel mit \ce{I^-} und Reduktionsmittel mit \ce{I^2} quantitativ bestimmen. Iod an sich ist in Wasser schlecht löslich.
In Gegenwart von Iodid bildet sich der triefbraune komplex Triiodid, welcher besser Löslich ist. Dieser könnte als Indikator genügen. Jedoch wird oft Stärke benutzt, da dieses mit Iod/Iodid einen intensiven tiefblauen komplex Bildet. Mit diesem können selbst sehr geringe Konzentrationen an Iod in einer Iodidlösung erkannt werden.
\subsubsection{Manganometrie}
Die Manganometrie oder auch Permanganometrie basiert auf dem starken Oxidations- mittel Permanganation \ce{MnO4^-}. Dieses hat in wässriger Lösung eine intensiv rotviolette Farbe und reagiert mit reduzierenden Stoffen in stark saurer Lösung zum farblosen \ce{Mn^{2+}}-Ion:
\begin{equation}
    \ce{MnO4^- + 8H3O^+ + 5e^- <=> Mn^{2+} + 12H2O}
\end{equation}
Es wird titriert bis die Lösung sich komplett entfärbt hat. Die Manganometrie dient häufig zur quantitativen Bestimmung von Eisen(II)-Ionen, Wasserstoffperoxid, Mangan(II)-Ionen und Oxalat in wässrigen Lösungen.
Die Manganometrie wird durch alle Substanzen gestört, die ebenfalls oxidierbar sind, z.B. Chlorid.
\subsubsection{Cerimetrie}
An Stelle von Permanganat kann man in den meisten Fällen auch eine Cer(IV)-Masslösung verwenden als oxidierenden Titer verwenden.
Vorteil: Muss nicht vor Licht geschützt werden und ist über einen langen Zeitraum stabil. Nachteil: Aus gelbem \ce{Ce^{4+}} wird farbloses \ce{Ce^{3+}} daher ist Eigenindikation schwierig. 
Die Titration wird im stark Saurem durchgeführt um eine Bildung von Cer(IV)-hydroxid zu vermeiden. Das Reduktionspotential beträgt +1.44V. Daher lassen sich viele Ionen oxidieren und titrimetrisch quantifizieren. Die Äquivalenzpunktbestimmung folgt potentiometrisch.
\subsubsection{Bromatometrie}
Bei der Bromatometrie wird die oxidierbare Probe mit einer Bromatlösung titriert und es laufen folgende Reaktionen ab:
\begin{equation}
    \ce{BrO3^- + 6 H3O^+ <=> Br^- + 9 H2O}
\end{equation}
Ist keine oxidierbare Probe mehr vorhanden, reagiert das Bromid mit weiterem Bromat unter Komproportionierung zu elementaren Brom:
\begin{equation}
    \ce{BrO3^- + 5Br + 6 H3O^+ <=> 3 Br2 + 9H2O}
\end{equation}
Sämtliche Reaktionen sind farblos, daher kann Brom nicht als Eigenindikator gebraucht werden. Als Indikator werden hier organische Farbstoffe eingestzt, die durch das entstehende Brom zerstört werden. Häufig dient Methylorange, welches sich bei der Reaktion mit Brom entfärbt, als Indikator.