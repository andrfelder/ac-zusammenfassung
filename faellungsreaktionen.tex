\section{Fällungsreaktionen}
Fällungsreaktionen und Kristallisationen sind Vorgänge, bei denen aus einer Lösung schwerer lösliche Verbindungen als Niederschläge ausfallen. Häufig dienen sie der Abtrennung von Produkten oder Nebenprodukten bei chemischen Reaktionen.
\subsection{Löslichkeitsprodukt}
Das Gleichgewicht zwischen gelöster Substanz und Festkörper wird durch das Löslichkeitsprodukt beschrieben.
Im Beispiel von Silberchlorid \ce{AgCl} wird es folgendermassen beschrieben:
\begin{equation}
    \ce{AgCl_{(s)} <=> Ag^+_{(aq)} + Cl^-_{(aq)}}
\end{equation}
mit der Gleichgewichtskonstanten:
\begin{equation}
    K' = \ce{\frac{[Ag^+] \cdot [Cl^-]}{[AgCl]}}
\end{equation}
Die Aktivität eines Festkörper in Lösung ist konstant. Daher wird das Löslichkeitsprodukt beschrieben durch:
\begin{equation}
    \ce{K_L = [Ag^+] \cdot [Cl^-] = K^' \cdot [AgCl_{(s)}]}
\end{equation}
\begin{shaded}
    Die allgemeine Form des Löslichkeitsprodukts wird beschrieben durch:
    \begin{equation}
        \begin{split}
            \ce{A_mB_n <=> m A^{n+} + n B^{m-}} \\
            \ce{K_L = [A^{n+}]^m \cdot [B^{m-}]^n}
        \end{split}
    \end{equation}
\end{shaded}
Ist \ce{K_L} $\leqq 0.1$ spricht man von schwerlöslich, bei \ce{K_L} $> 0.1$ von mässig löslich und bei \ce{K_L} $\geqq 10$ von leicht löslichen Verbindungen.
\subsection{Löslichkeit}
Die Löslichkeit eines Stoffes ist die maximale Konzentration, in der er unter Gleichgewichtsbedingungen in einem anderen Stoff (Lösemittel) noch gelöst (einphasig) ist. Wird diese Konzentration überschritten, so bildet sich ein Niederschlag (zweite Phase).
Die Löslichkeit eines Salzes kann man mit Hilfe des Löslichkeitsprodukts berechnen:
\begin{equation}
    L = \ce{\frac{[A^{n+}]}{m} = \frac{B^{m-}}{n}} = \sqrt[n+m]{\frac{\ce{K_L}}{\ce{m^m \cdot n^n}}}
\end{equation}
\subsection{Potentiometrische Titration von Halogeniden mit \ce{Ag^+}}
Bei der potentiometrischen Titration wird die konzentrationsabhängige elektromotorische Kraft (EMK) gemessen.
Die Konzentrationsabhängigkeit des Potentials kann zur quantitativen Bestimmung der Ionenkonzentration einer 
schwerlöslichen Salzes und dessen Löslichkeitsprodukt durch eine Fällungstitration verwendet werden. \\
Um dies durchzuführen wird die \ce{Ag^+}-Lösung vorgelegt und mit einer \ce{X^-}-Lösung bekannter Konzentration titriert.
Dabei wird dir Spannungsdifferenz gemessen. Es ergibt sich folgender Graph (hier \ce{X^- = Cl^-}):
\begin{figure}[!ht]
    \centering
    \begin{minipage}{.5\linewidth}
      \includegraphics[width=\linewidth]{Media/potentialDiagram.jpg}
    \end{minipage}
\end{figure}
Der Sprung erfolgt, wenn ein Äquivalent an \ce{Cl^-} zugegeben worden ist. An diesem Punkt entspricht die Menge 
von zugegeben \ce{Cl^-} also der Menge des anfangs vorliegenden \ce{Ag^+}. Die Kurve kann mit folgenden Gleichungen berechnet werden:
\begin{equation*}
   \begin{split}
	    \ce{[Ag^+]_0 &= [Ag^+] + [AgCl]} \\
        \ce{[Cl^-]_{zg} &= [Cl^-] + [AgCl]} \\
        \ce{[Cl^-] &= [Cl^-]_{zg} - ([Ag^+]_0 - [Ag^+])} \\
        \ce{K_L &= [Ag^+] \cdot [Cl^-] = [Ag^+] \cdot \{[Cl^-]_{zg} - ([Ag^+]_0 - [Ag^+]) \}}
    \end{split}
\end{equation*}
\subsection{Elektrolytische Leitfähigkeit}
Die Leitfähigkeit einer Lösung misst man, indem man den Widerstand in der Lösung in einer sogenannten
Leitfähigkeitszelle bestimmt. Diese Zelle besteht aus zwei planparallelen Elektroden.
\begin{shaded}
    Der gemessene Zellwiderstand ist zur Leitfähigkeit der Lösung umgekehrt proportional und besitzt daher 
    die Einheit Ohm$^{-1}$:
    \begin{equation}
        \Lambda = \frac{1}{R}
    \end{equation}
    mit $\Lambda$ als Leitfähigkeit in Siemens und R als elektrischer Widerstand in Ohm.\\
    Leitfähigkeit ist jedoch abhängi von der Distanz. Daher berechnet man oft die spezifische Leitfähigkeit:
    \begin{equation}
        \kappa = \Lambda \cdot \frac{I}{Q} = \Lambda \cdot f
    \end{equation}
    mit:
    \begin{itemize}
        \item $\kappa:$ als spezifische Leitfähigkeit in S $\cdot$ $cm^{-1}$
        \item $\Lambda:$ als Leitfähigkeit
        \item Q: Querschnitt des Strompfades
        \item I: Wirksamer Abstand der Elektroden
        \item f: Zellkonstante
    \end{itemize}
\end{shaded}
Bei gegebenem Lösungsmittel, konstanter Temperatur und konstantem Druck hängt der Ohm'sche Widerstand von Elektrolytlösungen ab von:
\begin{itemize}
    \item der Konzentration der geladenen Teilchen
    \item der Ladung der Teilchen
    \item der Mobilität der Teilchen
\end{itemize}
Um diese abhängigkeiten zu berücksichtigen, definiert man die molare Leitfähigkeit $\Lambda_C$:
\begin{equation}
    \Lambda_C = \frac{\kappa}{c}
\end{equation}
Die molare Leitfähigkeit ist konzentrationsabhängig. Daher wird zusätzlich noch die molare Grenzleitfähigkeit
eingeführt. Dies ist die molare Leitfähigkeit extrapoliert auf eine unendliche Verdünnung. Dies ermöglicht es,
molare Leitfähigkeiten zu vergleichen. 
\subsection{Konduktometrische Titration}
Die konduktometrische Titration wird anhand eines Beispiels erklärt. 100mL einer 0.01M NaCl-Lösung werden mit einer
1.0M \ce{AgNO3} Lösung titriert und die Leitfähigkeit wird gemessen. Während der Zugabe von \ce{AgNO3} bildet sich sofort
schwerlösliches Silberchlorid, solange noch freie Chloridionen in Lösung sind:
\begin{equation*}
    \ce{Ag^+ + Cl^- -> AgCl}
\end{equation*}
Die Fällung erfolgt fast quantitativ aufgrund der sehr kleinen Löslichkeit von Silberchlorid daher werden am Anfang 
der Titration \ce{Cl^-}-Ionen durch \ce{NO3^-}-Ionen ersetzt. Somit bleibt die Leitfähigkeit auch relativ konstant.
Ist das vorgelegte Chlorid aber ausgefällt, ergibt die weitere Zugabe von \ce{AgNO3} Lösung einen starken Anstieg der Leitfähigkeit.
Am Schnittpunkt der beiden Geraden kann der Äquivalenzpunkt ermittelt werden:
\begin{figure}[!ht]
    \centering
    \begin{minipage}{.5\linewidth}
      \includegraphics[width=\linewidth]{Media/diagrammLeit.jpg}
    \end{minipage}
\end{figure}
Die konduktometrische Titration ergibt im Allgemeinen nur dann gute Resultate, wenn in einem Teil der Titration ein schwerlöslicher Festkörper gebildet werden kann.

