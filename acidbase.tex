\section{Säuren und Basen}
\subsection{Definition nach Bronsted und Lowry}
Nach Bronsted und Lowry sind Säuren \textbf{Protonendonatoren} und Basen \textbf{Protonenakzeptoren}. Die Säure-Base-Reaktionen sind Protonenübertragunsreaktionen:
\begin{figure}[!ht]
    \centering
    \begin{minipage}{.5\linewidth}
      \includegraphics[width=\linewidth]{Media/sb-paar.png}
    \end{minipage}
  \end{figure}
\subsection{Das Massenwirkungsgesetz}
Die Geschwindigkeit von chemischen Reaktionen ist proportional zur Konzentration der Reaktanden, deshalb nimmt die Reaktionsgeschwindigkeit der Hinreaktion ab, wenn wir HA \& B
in einem Behälter vermischen und sich bereits $\text{A}^-$ und $\text{HB}^+$ gebildet haben. Je mehr $\text{A}^-$ und $\text{HB}^+$ gebildet wurden, umso schneller wird die Rückreaktion.
Ab einem gewissen Zeitpunkt setzt das chemische Gleichgewicht ein: Hinreaktion = Rückreaktion. Im Gleichgewichtszustand sind Konzentrationen der beteiligten Substanzen konstant.
Es handelt sich um ein dynamisches Gleichgewicht. Im Gleichgewicht existiert keine Triebkraft mehr, die die Zusammensetzung der Reaktionsmischung ändert. Dies bedeutet jedoch nicht,
dass die Einzelprozesse zum Stillstand gekommen sind.
\begin{shaded}
    Die \textbf{Gleichgewichtskonstante K} ist die Verhältniszahl der Konzentrationen, welche im Gleichgewicht herrschen. Für eine genaue Betrachtung der Verhältnisse wird lediglich die Konzentration der Reaktanden betrachtet,
    die effektiv an der Reaktion beteiligt sind (Aktivität a):
    \begin{equation}
        K = \frac{a_{\text{A}}^- \cdot a_{\text{HB}}^+}{a_{\text{HA}} \cdot a_{\text{B}}} = \frac{k_1}{k_{-1}}
    \end{equation}
    sowie gilt: 
    \begin{equation}
        k_1 \cdot a_{\text{HA}} \cdot a_{\text{B}} = k_{\text{-1}} \cdot  a_{\text{A}} \cdot a_{\text{HA}^+}
    \end{equation}
    Wobei $K_1$ und $K_{-1}$ die Geschwindigkeitskonstanten der Hin- \& Rückreaktion sind.
\end{shaded}
Das Massenwirkungsgesetz steht im Zusammenhang mit der Aktivität. Das Massenwirkungsgesetz sagt aus, dass das Produkt aus den Aktivitäten der beteiligten Stoffen konstant ist.
Die Konstante wird als $K$ bezeichnet. Der Allgemeine Ausdruck des Massenwirkungsgesetz ist folgendermassen:
\begin{align}
	\ce{aA + bB &<=> cC + dD} \\
    \text{Edukte} \ce{&<=>} \text{Produkte} \\
    \frac{K_1}{K_{-1}} = &\text{ K } = \frac{a_C^c \cdot a_D^d}{a_A^a \cdot a_B^b}
\end{align}
Für die Lage des Gleichgewichts gilt: 
\begin{itemize}
    \item K $>$ 1: GG liegt rechts $\rightarrow$ Reaktion läuft bevorzugt in Richtung Produkt ab.
    \item K $=$ 1: GG liegt in der Mitte. Alle Reaktionen liegen in ähnlichen Konzentrationen vor.
    \item K $<$ 1: GG liegt links $\rightarrow$ Rückreation lauft bevorzugt ab.
\end{itemize}
Dabei sind K und a \textbf{dimensionslos}. \\
Für die \textbf{Ionenstärke} $I$ in einer Lösung gilt:
\begin{equation}
    I = 0.5 \cdot \sum^n_{\text{i = 1}} c_i \cdot z_i^2
\end{equation}
Wobei $i$ für jede vorkommende Ionenart und $z$ für die jeweilige Ladung steht. Neben der Konzentration hat auch der Druck einen Einfluss auf das chemische Gleichgewicht:
\begin{equation}
    K_p = \frac{P_C^c \cdot P_D^d}{P_A^a \cdot p_B^b}
\end{equation}
Erhöt man den Druck eines Systems im Gleichgewicht, so weicht das System der Druckerhöhung aus und das Gleichgewicht wird auf die Seite der kleineren Teilchenzahl verschoben.\\
$K$ ist eine thermodynamische Grösse und steht mit der freien Reaktionsenthalpie folgendermassen in Beziehung:
\begin{equation}
    \Delta r G^0 = -RT\ln K \text{wobei} \Delta r G^0 = \Delta r H^0 - T\Delta r S^0
\end{equation}
Differentiert man nach der Temperatur, erhält man die van't Hoff'sche Reaktionsisobare:
\begin{equation}
    \left( \frac{\delta \ln K }{\delta T} \right)_P = \frac{\Delta rH^0}{RT^2}
\end{equation}
\begin{shaded}
    Bei \textbf{endothermen} Reaktionen ist $\Delta rH^0 > 0$ und $\delta \ln K > 0$ wenn $\delta T > 0$ ist. \\
    Bei Temperaturerhöhung: Wärmezufuhr $\rightarrow$ $K$ ist grösser $\rightarrow$ GG verschiebt sich nach rechts. \\
    Bei Temperatursenkung: GG verschiebt sich nach links. \\ \\
    Bei \textbf{exothermen} Reaktionen ist $\Delta rH^0 < 0$ und $\delta \ln K < 0$ wenn $\delta T < 0$ ist. \\
    Bei Temperaturerhöhung: Wärmezufuhr $\rightarrow$ $K$ ist grösser $\rightarrow$ GG verschiebt sich nach rechts. \\
    Bei Temperatursenkung: GG verschiebt sich nach links. \\
\end{shaded}
\textbf{Das Prinzip von Le Chatelier} sagt: Wird Druck, Temperatur oder Konzentration im System geändert, so verschiebt sich das Gleichgewicht derart,
dass es der Änderung ausweicht und es stellt sich ein neues Gleichgewicht ein mit weniger Zwang.
\subsection{Die Eigendissoziation des Wassers}
Wasser ist ein Ampholyt und kann somit als Säure und Base agieren. Wasser dissoziirt in geringem Masse zu \ce{H3O^+} und \ce{OH^-}. Dies wird Eigendissoziation oder Autoprotolyse genannt:
\begin{align}
    \ce{H2O + H2O &<=> H3O^+ + OH^-} \text{ Autoprotolyse} \\
    \ce{H3O^+ + OH^- &<=> 2H2O} \text{ Neutralisation}
\end{align}
Mit dem Massenwirkungsgesetz kann man aus dem Gleichgewicht $K$ für Wasser aufstellen:
\begin{equation}
    K = \frac{\ce{[H3O^+]} \cdot \ce{[OH^-]}}{\ce{[H2O]}^2}
\end{equation}
Das Ionenprodukt ist : $K_w = K[\ce{H2O}]^2 = [\ce{H3O^+}] \cdot [\ce{OH^-}]$ und hat den Wert $10^{-14}$. In reinem Wasser hat man Elektronenneutralität: $[\ce{H3O^+}] = [\ce{OH^-}] = \sqrt{K_w} = 10^{-7}$.
\subsection{Gleichgewichte im Wasser}
Die Dissoziation einer Säure in Wasser ist gegeben durch:
\begin{align}
    \ce{HA + H2O &<=> A^- + H3O^+} \\
    \ce{A^- + H2O &<=> HA + OH^-}
\end{align}
$K_s$ ist ein Mass für die Lage des Dissoziationsgleichgewichtes einer Säure und damit für die Stärke der Säure:
\begin{equation}
    \ce{K_s = K \cdot [H2O] = \frac{[A^-] \cdot [H3O^+]}{[HA]}}
\end{equation}
Für die Dissoziation einer Base in Wasser gilt:
\begin{equation}
    \ce{K_b = \frac{[HA] \cdot [OH^-]}{[A^-]}}
\end{equation}
Der Zusammenhang von \ce{K_s} und \ce{K_b} ist folgendermassen:
\begin{equation}
    \ce{K_s \cdot K_b = \frac{[A^-] \cdot [H3O^+]}{[HA]} \cdot \frac{[HA] \cdot [OH^-]}{[A^-]} = [H3O^+] \cdot [OH^-] = K_w}
\end{equation}
Somit gilt natürlich auch \ce{pK_s + pK_b = pK_w} sowie \ce{pK_s} $< 0$ eine starke Säure und \ce{pK_s} $> 0$ eine schwache Säure ist.
\subsection{Der pH Wert}
\begin{shaded}
    Die wichtigsten Formeln im Überblick:
    \begin{align}
        \ce{pH} &= -\log{\ce{[H3O^+]}} \\
        \ce{pK_s} &= -\log{\ce{K_s}} = -\log{\frac{\ce{[A^-] \cdot [H3O^+]}}{\ce{[HA]}}} = -\log{\frac{\ce{[A^-]}}{\ce{[HA]}}} - \log{\ce{[H3O^+]}} \\
        \ce{pK_s &= pH - \log{\frac{[A^-]}{[HA]}}} \rightarrow \text{Puffergleichung} \\
        \ce{pK_w &= -\log{10^{-14} = 14}}
    \end{align}
\end{shaded}
\subsection{Mehrprotonige Säuren}
Mehrprotonige Säuren sind Säuren, welche mehrere Protonen abgeben können:
\begin{align}
    \ce{H2A + H2O &<=> HA^- + H3O^+} \qquad \ce{pK_{s1} = -\log{K_{s1}} = -\log{\frac{[HA^-] \cdot [H3O^+]}{[H2A]}}} \\
    \ce{HA^- + H2O &<=> A^{2-} + H3O^+} \qquad \ce{pK_{s2} = -\log{K_{s2}} = -\log{\frac{[A^{2-}] \cdot [H3O^+]}{[HA]}}}
\end{align}
Gleichgewichtskonstante eines Indikators \ce{K_{HInd}} und der \ce{pK_s} Wert sind definiert als:
\begin{align}
    \ce{K_{HInd} &= \frac{[H^+] \cdot [Ind^-]}{[HInd]}} \\
    \ce{pK_{HInd} &= -\log{[H^+]} - \log{\frac{[Ind^-]}{[HInd]}}} \\
    \ce{pH &= pK_{HInd} + \log{\frac{[Ind^-]}{HInd}}} \\
    \ce{pH &= pK_{HInd} \pm 2}
\end{align}
\subsection{Bestimmung des Äquivalenzpunktes}
Der Äquivalenzpunkt entspricht bei Titration einer starken Säure mit starker Base dem Neutralisationspunkt pH = 7. Titriert man aber eine
schwache Säure mit einer starken Base, so fallen Äquivalenzpunkt und Neutralisationspunkt nicht mehr zusammen. Es gilt:
\begin{align}
    \ce{HA + OH^- &<=> A^- + H2O} \rightarrow \text{Titration einer schwachen Säure mit einer starken Base}\\
    \ce{K_b &= \frac{[HA] \cdot [OH^-]}{[A^-]}} \rightarrow \text{Die Eigendissoziation der Säure wird vernachlässigt.}
\end{align}
und somit:
\begin{align}
    \ce{pK_s + pK_b &= 14 \rightarrow pK_b = 14 - pK_s} \\
    \ce{K_b &= \frac{[OH^-]^2}{[A^-]_0} \rightarrow [OH^-] = \sqrt{K_b \cdot [A^-]_0} \text{ oder } pOH = 1/2(pK_b-\log{[A^-]_0})}
\end{align}
Wobei \ce{[A^-]_0} gleich der Konzentration des reinen Salzes am Äquivalenzpunkt ist. Der pH ergibt sich dann aus $\ce{pH = 14 - pOH}$.
Mit folgender Gleichung können auch pK-Werte von schwachen Säuren und Basen ermittelt werden:
\begin{equation}
    \ce{pH = pK_s + \log{\frac{[A^-]}{HA}} \text{ und } \log{\frac{[A^-]}{HA}} = 0 \text{ falls } [A^-] = [HA]}
\end{equation}
Für schwache Säuren gilt:
\begin{equation}
    \ce{pH = pK_s \text{ falls } [A^-] = [HA]}
\end{equation}
\subsection{Pufferlösungen}
Puffer sind wässrige Lösungen, die eine schwache Säure und deren konjugierte Base in ähnlicher Konzentration enthält. Diese Lösung
"puffert" einen pH-Bereich, sofern $\ce{[OH^-]_{zg}, [H^+]_{zg}}$ $<$  $\ce{[A^-], [HA]}$ \\
Der pH ist mithilfe von Gleichung (20) berechenbar. Die maximale Konzentration der Störung kann man berechnen, indem man zu einer 
Pufferlösung Protonen oder \ce{[OH]^-}-Ionen dazu gibt:
\begin{equation}
    \ce{pH = pK_s + \log{\frac{[A^-]_0 - [H3O^+]}{[HA]_0 + [H3O^+]}}}
\end{equation} 
\subsection{Zwitterionen}
Sind Verbindungen, die mehrere funktionelle Gruppen haben, von denen ein Teil negativ und der andere posiitiv geladen ist. Bei einem 
bestimmten pH-Wert, dem \textbf{isoelektrischen Punkt}, liegen gelich viele anionische wie Kationische Gruppen vor. Wenn der pH 
tiefer liegt, so wird weniger Säure dissoziiert. Der pH-Wert am IEP, bei dem die Konzentration der Anionen und Kationen gleich ist 
kann man mit folgender Gleichung berechnen:
\begin{equation}
    \ce{pH = \frac{pK_{ss} + pK_{sb}}{2}}
\end{equation}
Bei Titrationen weisen Aminosäuren 2 \ce{pK_s}-Werte als Sattelpunnkte in der Titrationskurve auf. Im Bereich dieser Sattelpunkte wirken die Aminosäuren als Puffer.
\subsection{pH-Messung mit pH-Metern}
Die pH-Messung mit pH-Meter ist eine potentiometrische Messung, daher kann diese nur erfolgen, indem das Potential einer Messelektrode
gegen das Potential einer Bezugselektrode gemessen wird. 
Zwei Metallleiter, die durch Elektrolyllösung miteinander verbunden sind, bilden eine galvanische Zelle.
Ein Messinstrument verbindet die beiden Leiter und die Kettspannung $U$ (Galvanispannung) kann 
gemessen werden. An Phasengrenzen der Elektroden einer galvanischen Kette findet ein Ladungsaustausch statt und dies führt zur Kettenspannung.
Die Kettenspannung setzt sich aus der Spannung der Bezugselektrode, die Diffusionsspannung und die Spannung der Messelektrode, welche von
der Konzentration der gemessen Ionen abhängt.
\begin{shaded}
    Die Zusammenhänge zwischen Konzentration und Kettenspannung ist in der \textbf{Nernst-Gleichung} dargestellt:
    \begin{equation}
        E = E^0 + \frac{RT}{zF} \ln{\frac{[\text{Ox}]}{[\text{Red}]}}
    \end{equation}
    mit: 
    \begin{align*}
        E&: \text{EMK der Zelle} \\
        E^0&: \text{EMK der Zelle bei Standartbedienungen} \\
        F&: \text{Farraday Konstante} \\
        R&: \text{Gaskonstante} \\
        z&: \text{Anzahl übertragener Elektronen} \\
        T&: \text{Absolute Temperatur}
    \end{align*}
\end{shaded}
Die Spannung der pH-Elektrode wird folgendermassen berechnet:
\begin{equation}
    E_{\text{Hz}} = E_{\text{innen}} - E_{\text{aussen}} = \frac{2.303 \cdot RT}{zF} \cdot \log{\frac{\ce{[H^+ aussen]}}{\ce{[H^+ innen]}} = 0.059(\ce{pH_{innen} - pH_{aussen}})}
\end{equation}
Das Potential lässt sich dann mithilfe der Nernst-Gleichung Berechnen (hier im bsp. mit \ce{Ag / AgCl}):
\begin{equation}
    \ce{E_{Ag / AgCl} = E^0_{Ag / AgCl} - \frac{2.303 \cdot RT}{1F} \cdot \log{[Cl^-]} = \text{konstant}}.
\end{equation}
Das gemessene Potential steht folgendermassen mit dem pH-Wert in Beziehung (nur im Idealfall, bei 25 Grad):
\begin{equation}
    \ce{-\log{[H^+]} = pH = \frac{-E_{gem} -E_{kal}}{0.059}}
\end{equation}